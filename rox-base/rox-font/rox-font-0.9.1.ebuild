# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/rox-base/mime-editor/mime-editor-0.5-r2.ebuild,v 1.3 2008/01/01 14:36:42 josejx Exp $

ROX_LIB_VER="2.0.4-r1"
inherit rox-0install

MY_PN="Font"
DESCRIPTION="Font is a font selection and smoothing configuration tool for ROX Desktop"
HOMEPAGE="http://rox.sourceforge.net/desktop/"
SRC_URI="mirror://sourceforge/rox/${MY_PN}-${PV}.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

APPNAME=${MY_PN}
APPCATEGORY="System;Core"
