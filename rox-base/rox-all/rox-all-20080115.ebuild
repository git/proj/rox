# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A meta-ebuild for installing the ROX Desktop, and many useful related packages"
HOMEPAGE="http://www.gentoo.org/proj/en/desktop/rox/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="acpi gnome wifi"

# TODO: Do something about the fact that rox-session's window-manager selection
# code isn't very intuitive.  And probably provide a choice in here to ensure
# that at least one window manager is installed!

RDEPEND="
	rox-base/mime-editor
	rox-base/pager
	rox-base/rox-autostart
	rox-base/rox-session
	rox-base/systemtrayn
	rox-base/tasklist
	rox-base/thumbs
	rox-base/volume
	rox-base/xdg-menu
	rox-extra/archive
	rox-extra/clock
	rox-extra/contacts
	rox-extra/diff
	rox-extra/downloadmanager
	rox-extra/edit
	rox-extra/find
	rox-extra/memo
	rox-extra/picky
	rox-extra/resolution
	rox-extra/reticker
	rox-extra/ripper
	rox-extra/roxcd
	rox-extra/roxdao
	rox-extra/roxiso
	rox-extra/rox-tail
	rox-extra/rox-xplanet
	rox-extra/wallpaper
	rox-extra/weather
	acpi? ( rox-extra/lithium )
	gnome? ( rox-base/devtray
		rox-base/tasktray
		rox-extra/gnome-thumbnailer )
	wifi? ( rox-extra/rox-wifi )
	rox-extra/comicthumb
	rox-extra/magickthumbnail
	rox-extra/videothumbnail
	"
# TODO: Put these last 3 thumbnailers in a USE flag of some sort?

# TODO: Add rox-extra/musicbox?  But fix it first.

pkg_postinst() {
	# TODO: Create default panel?
	# Home
	# Apps
	# XDG-Menu
	# ...
	# devtray
	# tasktray
	# memo
	# systrayn
	# pager

	# TODO: Create default application handlers for archive and other nice
	# format handlers?  Or perhaps do that in their own ebuilds?

	# TODO: Get xdgsettings or some other send-to manager in here and have it
	# autostart by default by rox-session?

	einfo "This is an experimental wrapper, please send suggestions,"
	einfo "improvements, or patches to rox@gentoo.org"
}
