# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/rox-base/mime-editor/mime-editor-0.5-r2.ebuild,v 1.3 2008/01/01 14:36:42 josejx Exp $

ROX_LIB_VER="2.0.4-r1"
inherit rox-0install

MY_PN="keyboard"
DESCRIPTION="Mouse is a rodent configuration tool for ROX Desktop"
HOMEPAGE="http://rox.sourceforge.net/desktop/Mouse"
SRC_URI="mirror://sourceforge/rox/${MY_PN}-${PV}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

APPNAME="Keyboard"
APPCATEGORY="System;Core"
S="${WORKDIR}/${MY_PN}-${PV}"

src_unpack() {
	unpack ${A}

	# This forces us to use 'python2.4' but we can trust that the 'python'
	# executable is at least 2.4, and may even be 2.5 or later!
	sed -i -e 's/python2.4/python/' "${S}/${APPNAME}/AppRun" || die "Could not patch python version"
}
