# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome-python-desktop

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~sh ~sparc ~x86 ~x86-fbsd"
IUSE="examples"

DEPEND=">=gnome-extra/nautilus-cd-burner-2.15.3"

src_install() {
	gnome-python-desktop_src_install

	if use examples; then
		insinto /usr/share/doc/${PF}/examples
		doins examples/nautilusburn/*
	fi
}

