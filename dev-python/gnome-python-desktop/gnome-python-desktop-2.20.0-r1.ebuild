# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome-python-desktop

MY_BINDING="gnomedesktop"
DESCRIPTION="META build which provides python interfacing modules for some GNOME desktop libraries"
HOMEPAGE="http://pygtk.org/"

LICENSE="LGPL-2.1 GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~sh ~sparc ~x86 ~x86-fbsd"
IUSE="metacity"

RDEPEND="=dev-python/gnome-desktop-applet-${PV}*
	=dev-python/gnome-desktop-gnomeprint-${PV}*
	=dev-python/gnome-desktop-gnomeprintui-${PV}*
	=dev-python/gnome-desktop-gtksourceview-${PV}*
	=dev-python/gnome-desktop-wnck-${PV}*
	=dev-python/gnome-desktop-totem_plparser-${PV}*
	=dev-python/gnome-desktop-gtop-${PV}*
	=dev-python/gnome-desktop-nautilusburn-${PV}*
	=dev-python/gnome-desktop-mediaprofiles-${PV}*
	=dev-python/gnome-desktop-rsvg-${PV}*
	metacity? ( =dev-python/gnome-desktop-metacity-${PV}* )
	=dev-python/gnome-desktop-gnomekeyring-${PV}*
	=dev-python/gnome-desktop-bugbuddy-${PV}*"

DEPEND=">=gnome-base/gnome-desktop-2.10.0"

src_install() {
	KEEP_PKGCONFIG="yes"
	gnome-python-desktop_src_install
}
