# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

#
# Original Author: Jim Ramsay <lack@gentoo.org>
# Purpose: Centralizes functions needed to split up the huge
# 'gnome-python-desktop' package into its constituent bindings
#
# Important enviroment variables:
#
# MY_BINDING: The actual '--enable-<binding>' name, which by default is the end
# 			  of ${PN} after the final '-'.  May be overridden if necessary.
#

inherit gnome2 python eutils autotools

MY_BINDING=${PN##*-}
MY_PN="gnome-python-desktop"
DESCRIPTION="provides python interfacing modules for some GNOME desktop libraries"
HOMEPAGE="http://pygtk.org/"
PVP=(${PV//[-\._]/ })
SRC_URI="mirror://gnome/sources/${MY_PN}/${PVP[0]}.${PVP[1]}/${MY_PN}-${PV}.tar.bz2
	http://dev.gentoo.org/~lack/patches/${MY_PN}-${PV}-split.patch.gz"

LICENSE="LGPL-2.1 GPL-2"
SLOT="0"

RDEPEND="virtual/python
	>=dev-python/pygtk-2.10.3
	>=dev-libs/glib-2.6.0
	>=x11-libs/gtk+-2.4.0
	>=dev-python/gnome-python-2.10.0
	!<dev-python/gnome-python-extras-2.13"
DEPEND="${RDEPEND}
	>=dev-util/pkgconfig-0.7"

DOCS="AUTHORS ChangeLog INSTALL MAINTAINERS NEWS README"

S="${WORKDIR}/${MY_PN}-${PV}"

gnome-python-desktop_src_unpack() {
	unpack ${A}
	cd "${S}"
	epatch "${WORKDIR}/gnome-python-desktop-${PV}-split.patch"
	eautoreconf
}

gnome-python-desktop_src_compile() {
	G2CONF="--disable-allbindings --enable-${MY_BINDING}"
	gnome2_src_compile
}

gnome-python-desktop_src_install() {
	gnome2_src_install

	# Only ONE package may install the pkgconfig file, so only set
	# KEEP_PKGCONFIG in that one ebuild (gnome-python-desktop)
	[[ -z $KEEP_PKGCONFIG ]] && \ 
		rm "${D}/usr/$(get_libdir)/pkgconfig/${MY_PN}-2.0.pc"
}

gnome-python-desktop_pkg_postinst() {
	python_version
	python_mod_optimize "${ROOT}/usr/$(get_libdir)/python${PYVER}/site-packages/gtk-2.0"
}

gnome-python-desktop_pkg_postrm() {
	python_version
	python_mod_cleanup
}

EXPORT_FUNCTIONS src_unpack src_compile src_install pkg_postinst pkg_postrm
